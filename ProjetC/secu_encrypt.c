// ProjetC.c
// AJC Formation
/*
*
secu_encrypt
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define LEN_CLEF 16
#define MAX_LEN 255
#define FALSE 0
#define TRUE  1

#define ASCII_MINUSCULE_OFFSET 97
#define ASCII_MINUSCULE_MAX 122
#define LONGUEUR_ALPHABET 26
#define TAILLE_MIN 1000
#define TAILLE_MAX 100000

typedef int Bool;

// D�claration des fonctions

int secu_encrypt(int argc, char* argv[]);


int main(int argc, char* argv[])
{ // debut main

    int CodeErr = secu_encrypt(argc, argv);

    if (CodeErr)
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
} // fin main

// Definition des Fonctions

int secu_encrypt(int argc, char* argv[])
{
    // D�claration de Variables

    int rtn_open;
    char erreurOpen[MAX_LEN];

    Bool Chiffrement = FALSE;

    int caractere;
    int LongueurCLEF = 1, MoitieCLEF;
    int ValPermute1 = 0;
    int ValPermute2 = 0;
    int TailleFichier;

    char clef[LEN_CLEF];
    char NomFichier[MAX_LEN];
    char NomFichierSortie[MAX_LEN];
    char remove[MAX_LEN] = "del -f "; // Faire une version UNIX avec un define sur l'OS

    char lettres[LONGUEUR_ALPHABET] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

    FILE* Fileptr;
    FILE* FileptrSortie;

    // Instructions

    if (argc < 6)
    {
        fprintf(stderr, "Erreur : Pas assez d'arguments en entree\n");
        return EXIT_FAILURE;
    }

    for (int i = 1; i < argc; i++) // On traite les arguments donn�es en entr�e du programme
    { // debut for
        if (strcmp(argv[i], "-e") == 0) Chiffrement = TRUE;
        else if (strcmp(argv[i], "-d") == 0) Chiffrement = FALSE;
        else if (strcmp(argv[i], "-k") == 0)
        {
            i++;
            LongueurCLEF = strlen(argv[i]);
            if (LongueurCLEF > 10)
            {
                fprintf(stderr, "Erreur longueur clef : %d\n", LongueurCLEF);
                return EXIT_FAILURE;
            }
            strcpy_s(clef, LEN_CLEF, argv[i]);
        }
        else if (strcmp(argv[i], "-f") == 0)
        {
            i++;
            strcpy_s(NomFichier, MAX_LEN, argv[i]);
        }
        else
        {
            fprintf(stderr, "Erreur lecture argument : %s\n", argv[i]);
            return EXIT_FAILURE;
        }
    }// fin for

    for (int j = 0; j < LongueurCLEF; j++)
    {
        caractere = (int)clef[j];
        if ((caractere > ASCII_MINUSCULE_MAX) || (caractere < ASCII_MINUSCULE_OFFSET))
        {
            fprintf(stderr, "Erreur clef : la clef %s contient le caractere interdit : %c\n", clef, caractere);
            return EXIT_FAILURE;
        }
    }

    MoitieCLEF = LongueurCLEF / 2;

    for (int k = 0; k < MoitieCLEF; k++)
    {
        for (int index = 0; index < LONGUEUR_ALPHABET; index++)
        {
            if (clef[k] == lettres[index])
                ValPermute1 += index + 1;
            if (clef[k + MoitieCLEF] == lettres[index])
                ValPermute2 += index + 1;
        }
    }

    if (Chiffrement)
    {
        strcpy_s(NomFichierSortie, MAX_LEN, NomFichier);
        strcat_s(NomFichierSortie, MAX_LEN, ".enc"); // On rajoute l'extension .enc
    }

    else
        strncpy_s(NomFichierSortie, MAX_LEN, NomFichier, strlen(NomFichier) - 4); // On retire l'extension .enc

    rtn_open = fopen_s(&Fileptr, NomFichier, "rb");

    if (rtn_open != 0) // Si on arrive pas a ouvrir le fichier on renvoie une erreur pour eviter un acces interdit en memoire
    {
        strerror_s(erreurOpen, MAX_LEN, errno);
        fprintf(stderr, "Erreur : %s\n", erreurOpen);
        return EXIT_FAILURE;
    }

    fopen_s(&FileptrSortie, NomFichierSortie, "wb");

    fseek(Fileptr, 0, SEEK_END); // On se place � la fin du fichier pour avoir la taille du fichier avec ftell
    TailleFichier = ftell(Fileptr);
    fseek(Fileptr, 0, SEEK_SET);

    if ((TailleFichier < TAILLE_MIN) || (TailleFichier > TAILLE_MAX))
    {
        fprintf(stderr, "Erreur taille fichier : le fichier %s est de taille : %d\n", NomFichier, TailleFichier);
        return EXIT_FAILURE;
    }

    for (int m = 0; m < TailleFichier; m++)
    {
        caractere = fgetc(Fileptr);
        fputc(caractere, FileptrSortie);
    }

    for (int l = 1; l < LongueurCLEF + 1; l++) // On fait les permutations
    {
        fseek(Fileptr, ValPermute2 * l - 1, SEEK_SET);
        caractere = fgetc(Fileptr);
        fseek(FileptrSortie, ValPermute1 * l - 1, SEEK_SET);
        fputc(caractere, FileptrSortie);
        fseek(Fileptr, ValPermute1 * l - 1, SEEK_SET);
        caractere = fgetc(Fileptr);
        fseek(FileptrSortie, ValPermute2 * l - 1, SEEK_SET);
        fputc(caractere, FileptrSortie);
    }

    fclose(Fileptr);
    fclose(FileptrSortie);

    strcat_s(remove, MAX_LEN, NomFichier);

    system(remove); // On supprimer le fichier source

    return EXIT_SUCCESS;
}

// Ex�cuter le programme�: Ctrl+F5 ou menu D�boguer�> Ex�cuter sans d�bogage
// D�boguer le programme�: F5 ou menu D�boguer�> D�marrer le d�bogage
// 
// Les arguments sont � passer dans : D�bogueur Windows local / Propri�t�s de d�bogage de #NomDeProjet / Param�tres de configuration / D�bogage / Arguments de la commande
