﻿// ProjetC.c
// AJC Formation

/*

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define LEN_CLEF 16
#define MAX_LEN 255
#define FALSE 0
#define TRUE  1

#define ASCII_MINUSCULE_OFFSET 97
#define ASCII_MINUSCULE_MAX 122
#define LONGUEUR_ALPHABET 26
#define TAILLE_MIN 1000
#define TAILLE_MAX 100000

#define SEED time(NULL)

#define ASCII_CHIFFRE_OFFSET 48
#define ASCII_MAJUSCULE_OFFSET 65
#define ASCII_MAJUSCULE_MAX 90

typedef int Bool;

// Déclaration des fonctions

int secu_encrypt(int argc, char* argv[]);

int secu_generator(int argc, char* argv[]);

int secu_wc(int argc, char* argv[]);


// Utiliser le preprossessing pour generer la bonne fonction avec le main associé ==> créer un executable différent 
// ==> utiliser des fichiers source et entetes differents pour le makefile = generer un exec par source

#define ENC  1
#define GEN  2
#define WC   3
#define PROG WC

int main(int argc, char* argv[])
{ // debut main

#if PROG == ENC
    
    int CodeErr = secu_encrypt(argc, argv);

#elif PROG == GEN

    int CodeErr = secu_generator(argc, argv);

#else

    int CodeErr = secu_wc(argc, argv);

#endif

    if (CodeErr)
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
} // fin main

// Definition des Fonctions

int secu_encrypt(int argc, char* argv[])
{
    // Déclaration de Variables

    int rtn_open;
    char erreurOpen[MAX_LEN];

    Bool Chiffrement = FALSE;

    int caractere;
    int LongueurCLEF = 1, MoitieCLEF;
    int ValPermute1 = 0;
    int ValPermute2 = 0;
    int TailleFichier;

    char clef[LEN_CLEF];
    char NomFichier[MAX_LEN];
    char NomFichierSortie[MAX_LEN];
    char remove[MAX_LEN] = "del -f "; // Faire une version UNIX

    char lettres[LONGUEUR_ALPHABET] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

    FILE* Fileptr;
    FILE* FileptrSortie;

    // Instructions

    if (argc < 6)
    {
        fprintf(stderr, "Erreur : Pas assez d'arguments en entree\n");
        return EXIT_FAILURE;
    }

    for (int i = 1; i < argc; i++)
    { // debut for
        if (strcmp(argv[i], "-e") == 0) Chiffrement = TRUE;
        else if (strcmp(argv[i], "-d") == 0) Chiffrement = FALSE;
        else if (strcmp(argv[i], "-k") == 0)
        {
            i++;
            LongueurCLEF = strlen(argv[i]);
            if (LongueurCLEF > 10)
            {
                fprintf(stderr, "Erreur longueur clef : %d\n", LongueurCLEF);
                return EXIT_FAILURE;
            }
            strcpy_s(clef, LEN_CLEF, argv[i]);
        }
        else if (strcmp(argv[i], "-f") == 0)
        {
            i++;
            strcpy_s(NomFichier, MAX_LEN, argv[i]);
        }
        else
        {
            fprintf(stderr, "Erreur lecture argument : %s\n", argv[i]);
            return EXIT_FAILURE;
        }
    }// fin for

    for (int j = 0; j < LongueurCLEF; j++)
    {
        caractere = (int)clef[j];
        if ((caractere > ASCII_MINUSCULE_MAX) || (caractere < ASCII_MINUSCULE_OFFSET))
        {
            fprintf(stderr, "Erreur clef : la clef %s contient le caractere interdit : %c\n", clef, caractere);
            return EXIT_FAILURE;
        }
    }

    MoitieCLEF = LongueurCLEF / 2;

    for (int k = 0; k < MoitieCLEF; k++)
    {
        for (int index = 0; index < LONGUEUR_ALPHABET; index++)
        {
            if (clef[k] == lettres[index])
                ValPermute1 += index + 1;
            if (clef[k + MoitieCLEF] == lettres[index])
                ValPermute2 += index + 1;
        }
    }

    if (Chiffrement)
    {
        strcpy_s(NomFichierSortie, MAX_LEN, NomFichier);
        strcat_s(NomFichierSortie, MAX_LEN, ".enc");
    }

    else
        strncpy_s(NomFichierSortie, MAX_LEN, NomFichier, strlen(NomFichier) - 4);

    rtn_open = fopen_s(&Fileptr, NomFichier, "rb");

    if (rtn_open != 0)
    {
        strerror_s(erreurOpen, MAX_LEN, errno);
        fprintf(stderr, "Erreur : %s\n", erreurOpen);
        return EXIT_FAILURE;
    }

    fopen_s(&FileptrSortie, NomFichierSortie, "wb");

    // Inversion octets

    fseek(Fileptr, 0, SEEK_END);
    TailleFichier = ftell(Fileptr);
    fseek(Fileptr, 0, SEEK_SET);
    
    if ((TailleFichier < TAILLE_MIN) || (TailleFichier > TAILLE_MAX))
    {
        fprintf(stderr, "Erreur taille fichier : le fichier %s est de taille : %d\n", NomFichier, TailleFichier);
        return EXIT_FAILURE;
    }
    
    for (int m = 0; m < TailleFichier; m++)
    {
        caractere = fgetc(Fileptr);
        fputc(caractere, FileptrSortie);
    }

    for (int l = 1; l < LongueurCLEF + 1; l++)
    {
        fseek(Fileptr, ValPermute2 * l - 1, SEEK_SET);
        caractere = fgetc(Fileptr);
        fseek(FileptrSortie, ValPermute1 * l - 1, SEEK_SET);
        fputc(caractere, FileptrSortie);
        fseek(Fileptr, ValPermute1 * l - 1, SEEK_SET);
        caractere = fgetc(Fileptr);
        fseek(FileptrSortie, ValPermute2 * l - 1, SEEK_SET);
        fputc(caractere, FileptrSortie);
    }

    // Fermeture fichiers

    fclose(Fileptr);
    fclose(FileptrSortie);

    strcat_s(remove, MAX_LEN, NomFichier);

    system(remove);

    return EXIT_SUCCESS;
}

int secu_generator(int argc, char* argv[])
{
    // Déclaration de Variables

    int rtn_open;

    Bool Nombre = FALSE;
    Bool Hexa = FALSE;
    Bool Caracteres = FALSE;
    Bool Binaires = FALSE;
    Bool EcrireFichier = FALSE;
    
    int NombreElements;
    int RandInt;

    char NomFichier[MAX_LEN];

    FILE* Fileptr;

    // Initialisation

    srand(SEED);

    // Instructions

    if (argc < 3)
    {
        fprintf(stderr, "Erreur : Pas assez d'arguments en entree\n");
        return EXIT_FAILURE;
    }

    for (int i = 1; i < argc; i++)
    { // debut for

        if (strcmp(argv[i], "-c") == 0)
        {
            Caracteres = TRUE;
            i++;
            NombreElements = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-n") == 0) 
        {
            Nombre = TRUE;
            i++;
            NombreElements = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-b") == 0)
        {
            Binaires = TRUE;
            i++;
            NombreElements = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-h") == 0)
        {
            Hexa = TRUE;
            i++;
            NombreElements = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-f") == 0)
        {
            EcrireFichier = TRUE;
            i++;
            strcpy_s(NomFichier, MAX_LEN, argv[i]);
        }
        else
        {
            fprintf(stderr, "Erreur lecture argument : %s\n", argv[i]);
            return EXIT_FAILURE;
        }
    }// fin for

    if (EcrireFichier)
    {
        fopen_s(&Fileptr, NomFichier, "w");

        for (int m = 0; m < NombreElements; m++)
        { // debut for
            if (Caracteres)
            {
                RandInt = (rand() % (ASCII_MINUSCULE_MAX - ASCII_MAJUSCULE_OFFSET + 1)) + ASCII_MAJUSCULE_OFFSET;
                if ((RandInt > ASCII_MAJUSCULE_MAX) && (RandInt < ASCII_MINUSCULE_OFFSET))
                    RandInt += 10;
            }
            else if (Nombre)
            {
                RandInt = (rand() % 10) + ASCII_CHIFFRE_OFFSET;
            }
            else if (Hexa)
            {
                RandInt = (rand() % 16);
            }
            else if (Binaires)
            {
                RandInt = (rand() % 256);
            }
            else
            {
                fprintf(stderr, "Erreur generation elements\n");
                return EXIT_FAILURE;
            }
            fputc(RandInt, Fileptr);
        } // fin for

        fclose(Fileptr);
    }
    else
    {
        for (int m = 0; m < NombreElements; m++)
        { // debut for
            if (Caracteres)
            {
                RandInt = (rand() % (ASCII_MINUSCULE_MAX - ASCII_MAJUSCULE_OFFSET)) + ASCII_MAJUSCULE_OFFSET;
                if ((RandInt > ASCII_MAJUSCULE_MAX) && (RandInt < ASCII_MINUSCULE_OFFSET))
                    RandInt += 10;
            }
            else if (Nombre)
            {
                RandInt = (rand() % 10) + ASCII_CHIFFRE_OFFSET;
            }
            else if (Hexa)
            {
                RandInt = (rand() % 16);
            }
            else if (Binaires)
            {
                RandInt = (rand() % 256);
            }
            else
            {
                fprintf(stderr, "Erreur generation elements\n");
                return EXIT_FAILURE;
            }
            putc(RandInt, stdout);
        } // fin for
        printf("\n");
    }

    return EXIT_SUCCESS;
}

int secu_wc(int argc, char* argv[])
{
    int rtn_open;
    char erreurOpen[MAX_LEN];
    
    Bool CaracteresOnly = FALSE;

    int caractere = '#';

    int NombreLignes = 1;
    int NombreMots = 1;
    int NombreCaracteres = 0;

    char NomFichier[MAX_LEN];

    FILE* Fileptr;

    if (argc < 2)
    {
        fprintf(stderr, "Erreur : Pas assez d'arguments en entree\n");
        return EXIT_FAILURE;
    }

    for (int i = 1; i < argc; i++)
    { // debut for

        if (strcmp(argv[i], "-c") == 0)
        {
            CaracteresOnly = TRUE;
            i++;
            strcpy_s(NomFichier, MAX_LEN, argv[i]);
        }
        else
        {
            strcpy_s(NomFichier, MAX_LEN, argv[i]);
        }
    }// fin for

    rtn_open = fopen_s(&Fileptr, NomFichier, "r");

    if (rtn_open != 0)
    {
        strerror_s(erreurOpen, MAX_LEN, errno);
        fprintf(stderr, "Erreur : %s\n", erreurOpen);
        return EXIT_FAILURE;
    }

    while (caractere != EOF)
    {
        caractere = fgetc(Fileptr);
        
        switch (caractere)              // On considère que le retour à la ligne et l'espace sont des caractères et qu'un mot ne s'étend pas sur 2 lignes
        {
        case '\n':
            NombreLignes++;
            NombreMots++;
            NombreCaracteres++;
            break;
        case ' ':
            NombreMots++;
            NombreCaracteres++;
            break;
        default:
            NombreCaracteres++;
            break;
        } // fin switch
    }// fin while

    fclose(Fileptr);

    if (CaracteresOnly) printf("%d %s\n", NombreCaracteres, NomFichier);
    else printf("%d %d %d %s\n", NombreLignes, NombreMots, NombreCaracteres, NomFichier);

    return EXIT_SUCCESS;
}


// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage
// 
// Les arguments sont à passer dans : Débogueur Windows local / Propriétés de débogage de #NomDeProjet / Paramètres de configuration / Débogage / Arguments de la commande

*/