// ProjetC.c
// AJC Formation
/*
*
Partie 1 :

Dans un cadre de renforcement sécuritaire de son infrastructure, la société SecU vous charge de développer une bibliothèque de fonctions en langage C.

generate_password
is_password_strong
cypher_rotate
get_file_header
is_file_png
html_minifier

generate_password
Entrée: longueur du mot de passe, présence de caractères spéciaux (0/1)
Sortie: chaîne de caractères aléatoire contenant des lettres de l’alphabet (minuscule et majuscule), des chiffres et, selon paramétrage, des caractères spéciaux.
Exemple d’utilisation: generate_password(10, 1) => pM2ga_bn1#

is_password_strong
Entrée: chaîne de caractères
Sortie: 1 si la chaîne réunit toutes ces conditions:

de longueur 10 au moins
contient au moins une majuscule
contient au moins un chiffre
contient au moins un caractère spécial
0 sinon.
Exemple d’utilisation: is_password_strong(“abc”) => 0

cypher_rotate
Implémentation du chiffrement par décalage/rotation (cf chiffre de César)
Entrée: chaîne de caractères, sens de la rotation, longueur du décalage
Sortie: chaîne de caractères avec décalage des caractères
La rotation ne se fera que pour les lettres de l’alphabet minuscules.
Exemple d’utilisation: cypher_rotate(“ab2aZ”, 0, 1) => “bc2bZ”
Lien (Chiffre de César en python): https://www.tutorialspoint.com/cryptography_with_python/cryptography_with_python_caesar_cipher.htm

get_file_header
Entrée: fichier binaire
Sortie: format de fichier trouvé (parmi ceux recherchés)
On se limitera à rechercher les formats de fichier suivants: jpg, png, exe, pdf, doc
Lien: https://en.wikipedia.org/wiki/List_of_file_signatures

is_file_png
Entrée: fichier binaire
Sortie: 1 si le fichier est au format PNG, 0 sinon

html_minifier
Entrée: fichier html avec indentations et sauts de ligne
Sortie: fichier html sans indentations et sans sauts de ligne
Vous pouvez tester la fonction sur le fichier projet.html présent dans le dépôt
*
*/

#define GO 1
#define NO_GO 0

#if NO_GO

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define LEN_MDP 32
#define MAX_CHAINE 255

#define SEED time(NULL)

#define ASCII_OFFSET 33
#define ASCII_CHIFFRE_OFFSET 47
#define ASCII_CHIFFRE_MAX 58
#define ASCII_MAJUSCULE_OFFSET 64
#define ASCII_MAJUSCULE_MAX 91
#define ASCII_MINUSCULE_OFFSET 96
#define ASCII_MINUSCULE_MAX 123

#define SUCESS_STRONG 1
#define FAILURE_STRONG 0

#define NB_BYTE_CHECK 3
#define NB_EXTENSION 5

#define TRUE 1
#define FALSE 0

typedef enum Extension {
    _JPG,
    _PNG,
    _EXE,
    _PDF,
    _DOC,
    _AUTRE
} Extension;

// Déclaration des fonctions

void generate_password(char Password[], int length, int NbSpeChar); // Génère un mot de passe fort de la longueur souhaité par l'utilisateur

int is_password_strong(char Password[]); // Vérifie que le Mot de Passe est fort

void cypher_rotate(char ChaineEntree[], int sensRotation, int decalage, char ChaineSortie[]); // Decale les lettres minuscules avec un decalage et un sens definies en arguments

Extension get_file_header(FILE* file); // Faire un check extension == header // Enum

int is_file_png(FILE* file);

void html_minifier(FILE* file);


int main(int argc, char* argv[])
{
    // Déclaration de Variables

    char Caractere;
    int LongueurMDP, NombreCaractereSpeciaux;
    int CodeErrStrong;

    char MotDePasse[LEN_MDP];
    char ChaineEntree[MAX_CHAINE] = "azerty12QWERTY!?";
    char ChaineSortie[MAX_CHAINE];

    char* NomFichier = "poke.png";
    FILE* Fileptr;

    char* NomFichier2 = "projet.html";
    FILE* File2ptr;

    // Instructions

    printf("Saisir la longueur du mot de passe choisi ( au moins 10 ) : ");
    scanf_s("%d", &LongueurMDP);
    
    if (LongueurMDP < 10)
    {
        printf("Il faut au moins 10\n");
        return EXIT_FAILURE;
    }
        
    printf("Saisir s'il doit y avoir un caracteres speciaux choisis (Oui = 1 / Non = 0) : ");
    scanf_s("%d", &NombreCaractereSpeciaux);
    
    if ( (NombreCaractereSpeciaux != 0) && (NombreCaractereSpeciaux != 1) )
    {
        printf("Il faut Oui = 1 / Non = 0\n");
        return EXIT_FAILURE;
    }

    generate_password(MotDePasse, LongueurMDP, NombreCaractereSpeciaux);

    printf("\nLe mot de passe genere est : %s\n", MotDePasse);

    // ================================================================= //

    CodeErrStrong = is_password_strong(MotDePasse);

    printf("Le code erreur de is_password_strong est : %d\n", CodeErrStrong);

    // ================================================================= //

    cypher_rotate(ChaineEntree, 0, 15, ChaineSortie);
    //cypher_rotate(ChaineEntree, 1, 15, ChaineSortie);

    printf("La chaine %s est encode telle que : %s\n", ChaineEntree, ChaineSortie);

    // ================================================================= //

    fopen_s(&Fileptr, NomFichier, "rb");

    Extension Check_extension;

    Check_extension = get_file_header(Fileptr);

    printf("Extension Fichier : %d\n", Check_extension);

    fclose(Fileptr);

    // ================================================================= //

    fopen_s(&Fileptr, NomFichier, "rb");

    if (is_file_png(Fileptr)) printf("Le fichier est un PNG\n");
    else printf("Le fichier n'est pas un PNG\n");

    fclose(Fileptr);

    // ================================================================= //

    fopen_s(&File2ptr, NomFichier2, "r");

    html_minifier(File2ptr);

    fclose(File2ptr);

    return EXIT_SUCCESS;
}

// Definition des Fonctions

// Pas ok car pas forcement chiffres, ni maj, ni min
void generate_password(char Password[], int length, int NbSpeChar)
{
    // Déclaration de Variables
    
    int RandInt, i;

    int caractere, SpeCharOk = 0, MajCharOk = 0, ChiffreOk = 0, MinCharOk = 0;

    // Initialisation

    srand(SEED);                // Seed pour la fonction srand()

    // Instruction

    for (i = 0; i < length; i++)
    {//debut for
        
        RandInt = (rand() % 93) + ASCII_OFFSET;

        if ((RandInt > ASCII_CHIFFRE_OFFSET) && (RandInt < ASCII_CHIFFRE_MAX))  ChiffreOk = 1;
        if ((RandInt > ASCII_MAJUSCULE_OFFSET) && (RandInt < ASCII_MAJUSCULE_MAX))  MajCharOk = 1;
        if ((RandInt > ASCII_MINUSCULE_OFFSET) && (RandInt < ASCII_MINUSCULE_MAX))  MinCharOk = 1;

            switch (RandInt)
            {//debut switch
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 123:
            case 124:
            case 125:
            case 126:
                if (NbSpeChar == 1)
                {
                    Password[i] = (char)RandInt;
                    SpeCharOk = 1;
                }
                else
                {
                    i--;
                }
                break;
            default:
                Password[i] = (char)RandInt;
                break;
            }//fin switch
    }//fin for

    if ( (SpeCharOk == 0) && (NbSpeChar == 1) )
    {
        RandInt = (rand() % 14) + ASCII_OFFSET;
        int index = rand() % length;
        Password[index] = (char)RandInt;
    }

    if (ChiffreOk == 0)
    {
        RandInt = (rand() % 10) + ASCII_CHIFFRE_OFFSET + 1;
        int index = rand() % length;
        Password[index] = (char)RandInt;
    }

    if (MajCharOk == 0)
    {
        RandInt = (rand() % 26) + ASCII_MAJUSCULE_OFFSET + 1;
        int index = rand() % length;
        Password[index] = (char)RandInt;
    }

    if (MinCharOk == 0)
    {
        RandInt = (rand() % 26) + ASCII_MINUSCULE_OFFSET + 1;
        int index = rand() % length;
        Password[index] = (char)RandInt;
    }

    Password[i] = '\0';

    return;
}

int is_password_strong(char Password[])
{
    // Déclaration de Variables

    int caractere, SpeCharOk = 0, MajCharOk = 0, ChiffreOk = 0;
    int len = strlen(Password);

    // Instructions

    if (len < 10) return FAILURE_STRONG;

    for (int i = 0; i < len; i++)
    {//debut for
        caractere = (int)Password[i];

        if ((caractere > ASCII_CHIFFRE_OFFSET) && (caractere < ASCII_CHIFFRE_MAX))  ChiffreOk = 1;
        else if ((caractere > ASCII_MAJUSCULE_OFFSET) && (caractere < ASCII_MAJUSCULE_MAX))  MajCharOk = 1;
        else
        {//debut else
            switch (caractere)
            {//debut switch
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 123:
            case 124:
            case 125:
            case 126:
                SpeCharOk = 1;
                break;
            default:
                break;
            }//fin switch
        }//fin else
    }//fin for

    if (ChiffreOk && MajCharOk && SpeCharOk) return SUCESS_STRONG;

    return FAILURE_STRONG;
}

void cypher_rotate(char ChaineEntree[], int sensRotation, int decalage, char ChaineSortie[])
{
    int caractere;
    int length = strlen(ChaineEntree);

    for (int i = 0; i < length + 1; i++)
    {
        caractere = (int)ChaineEntree[i];

        if ((caractere > ASCII_MINUSCULE_OFFSET) && (caractere < ASCII_MINUSCULE_MAX))
        {
            caractere -= ASCII_MINUSCULE_OFFSET + 1; // Je ramene à une valeur entre 0 et 25
            if (sensRotation) caractere -= decalage;
            else caractere += decalage;
            if (caractere < 0) caractere += 26;
            caractere %= 26;
            caractere += ASCII_MINUSCULE_OFFSET + 1; // Je ramene à une valeur entre 97 et 122 pour coincider avec la table ascii
            ChaineSortie[i] = caractere;
        }
        else ChaineSortie[i] = ChaineEntree[i];
    }
    return;
}

Extension get_file_header(FILE * file) // Faire un check extension == header // Enum
{
    char chaine[NB_BYTE_CHECK];
    char JPG[NB_BYTE_CHECK] = { 255, 216, '\0' }; // jpg ==> FF D8
    char PNG[NB_BYTE_CHECK] = { 137, 80, '\0' }; // png ==> 89 50
    char EXE[NB_BYTE_CHECK] = { 77, 90, '\0' }; // exe ==> 4D 5A // 5A 4D
    char PDF[NB_BYTE_CHECK] = { 37, 80, '\0' }; // pdf ==> 25 50
    char DOC[NB_BYTE_CHECK] = { 208, 207, '\0' }; // doc ==> D0 CF

    fgets(chaine, NB_BYTE_CHECK, file);

    Extension Ext;

    if ( strcmp(chaine, JPG) == 0 ) Ext = _JPG;
    else if (strcmp(chaine, PNG) == 0) Ext = _PNG;
    else if (strcmp(chaine, EXE) == 0) Ext = _EXE;
    else if (strcmp(chaine, PDF) == 0) Ext = _PDF;
    else if (strcmp(chaine, DOC) == 0) Ext = _DOC;
    else Ext = _AUTRE;

    return Ext;
}

int is_file_png(FILE* file)
{
    char chaine[NB_BYTE_CHECK];
    char PNG[NB_BYTE_CHECK] = { 137, 80, '\0' }; // png ==> 89 50

    fgets(chaine, NB_BYTE_CHECK, file);

    if (strcmp(chaine, PNG) == 0) return TRUE;
    else return FALSE;
}

void html_minifier(FILE* file) // on peut créer un html autre : html_src != html_dst
{
    // Déclaration de Variables

    char caractere = ' ';

    char chaine1[MAX_CHAINE];
    char chaine2[MAX_CHAINE];

    char* NomFichier = "sortie.html";

    FILE* File2ptr;

    // Instructions

    fopen_s(&File2ptr, NomFichier, "w");

    while (caractere != EOF)
    {
        caractere = fgetc(file);
        if ((caractere == '\t') || (caractere == '\n') || (caractere == '\r')); // Ne fonctionne pas car il faut des espaces pour la compréhension des balises en html
        else fputc(caractere, File2ptr);
    }

    fclose(File2ptr);

    return;
}
#endif

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage
// 
// Les arguments sont à passer dans : Débogueur Windows local / Propriétés de débogage de #NomDeProjet / Paramètres de configuration / Débogage / Arguments de la commande