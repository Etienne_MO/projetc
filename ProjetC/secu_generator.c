﻿// ProjetC.c
// AJC Formation
/*
*
secu_generator
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_LEN 255
#define FALSE 0
#define TRUE  1

#define ASCII_MINUSCULE_OFFSET 97
#define ASCII_MINUSCULE_MAX 122

#define SEED time(NULL)

#define ASCII_CHIFFRE_OFFSET 48
#define ASCII_MAJUSCULE_OFFSET 65
#define ASCII_MAJUSCULE_MAX 90

typedef int Bool;

// Déclaration des fonctions

int secu_generator(int argc, char* argv[]);


int main(int argc, char* argv[])
{ // debut main

    int CodeErr = secu_generator(argc, argv);

    if (CodeErr)
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
} // fin main

// Definition des Fonctions

int secu_generator(int argc, char* argv[])
{
    // Déclaration de Variables

    Bool Nombre = FALSE;
    Bool Hexa = FALSE;
    Bool Caracteres = FALSE;
    Bool Binaires = FALSE;
    Bool EcrireFichier = FALSE;

    int NombreElements;
    int RandInt;

    char NomFichier[MAX_LEN];

    FILE* Fileptr;

    // Initialisation

    srand(SEED);

    // Instructions

    if (argc < 3)
    {
        fprintf(stderr, "Erreur : Pas assez d'arguments en entree\n");
        return EXIT_FAILURE;
    }

    for (int i = 1; i < argc; i++) // On traite les arguments données en entrée du programme
    { // debut for

        if (strcmp(argv[i], "-c") == 0)
        {
            Caracteres = TRUE;
            i++;
            NombreElements = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-n") == 0)
        {
            Nombre = TRUE;
            i++;
            NombreElements = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-b") == 0)
        {
            Binaires = TRUE;
            i++;
            NombreElements = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-h") == 0)
        {
            Hexa = TRUE;
            i++;
            NombreElements = atoi(argv[i]);
        }
        else if (strcmp(argv[i], "-f") == 0)
        {
            EcrireFichier = TRUE;
            i++;
            strcpy_s(NomFichier, MAX_LEN, argv[i]);
        }
        else
        {
            fprintf(stderr, "Erreur lecture argument : %s\n", argv[i]);
            return EXIT_FAILURE;
        }
    }// fin for

    if (EcrireFichier) // On ecrit dans le fichier
    {
        fopen_s(&Fileptr, NomFichier, "w");

        for (int m = 0; m < NombreElements; m++)
        { // debut for
            if (Caracteres)
            {
                RandInt = (rand() % (ASCII_MINUSCULE_MAX - ASCII_MAJUSCULE_OFFSET + 1)) + ASCII_MAJUSCULE_OFFSET;
                if ((RandInt > ASCII_MAJUSCULE_MAX) && (RandInt < ASCII_MINUSCULE_OFFSET))
                    RandInt += 10;
            }
            else if (Nombre)
            {
                RandInt = (rand() % 10) + ASCII_CHIFFRE_OFFSET;
            }
            else if (Hexa)
            {
                RandInt = (rand() % 16);
            }
            else if (Binaires)
            {
                RandInt = (rand() % 256);
            }
            else
            {
                fprintf(stderr, "Erreur generation elements\n");
                return EXIT_FAILURE;
            }
            fputc(RandInt, Fileptr);
        } // fin for

        fclose(Fileptr);
    }
    else                // On ecrit dans la console
    {
        for (int m = 0; m < NombreElements; m++)
        { // debut for
            if (Caracteres)
            {
                RandInt = (rand() % (ASCII_MINUSCULE_MAX - ASCII_MAJUSCULE_OFFSET)) + ASCII_MAJUSCULE_OFFSET;
                if ((RandInt > ASCII_MAJUSCULE_MAX) && (RandInt < ASCII_MINUSCULE_OFFSET))
                    RandInt += 10;
            }
            else if (Nombre)
            {
                RandInt = (rand() % 10) + ASCII_CHIFFRE_OFFSET;
            }
            else if (Hexa)
            {
                RandInt = (rand() % 16);
            }
            else if (Binaires)
            {
                RandInt = (rand() % 256);
            }
            else
            {
                fprintf(stderr, "Erreur generation elements\n"); // Je pense que ça n'arrive jamais mais au cas où
                return EXIT_FAILURE;
            }
            putc(RandInt, stdout);
        } // fin for
        printf("\n");
    }

    return EXIT_SUCCESS;
}


// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage
// 
// Les arguments sont à passer dans : Débogueur Windows local / Propriétés de débogage de #NomDeProjet / Paramètres de configuration / Débogage / Arguments de la commande
